# Comment fonctionne une architecture MVC ?

En programmation, certains problème deviennent tellement récurent qu'ils font l'objet de *design pattern*, des bonnes pratiques que d'autres développeur ont pris avant nous. Le pattern *MVC* est un des plus connus.
Il signifie:

- *M*odèle: la partie qui récupère les données brutes de la base de données, de les organiser et de les assembler.
- *V*ue: la partie qui gére l'affichage des données;
- *C*ontrôleur: la partie qui prend des décisions à partir des données du modèle et décide de quelle vue doit être exécutée et avec quelles données.

# Créer un Template de Page
La première étape de la création d'une template est la division de la page en bloc afin de factoriser l'appel de ces blocs entre les pages. Ainsi on va découpé la page en 'header', 'main' et 'footer' par exemple, pour que tous les 'header' et 'footer' soient identiques en faisant appel au même fichier 'header.php' ou 'footer.php'.

La deuxième étape est la création de la template en elle même en créant un fichier de base appelant les différentes parties de la vues. Pour rendre les blocs HTML plus pratiques à insérer, on va utiliser les fonctions PHP `ob_start()` et `ob_get_clean()`.

# Créer un routeur
Un routeur est une page (en général `indedx.php`) qui va récupérer toutes les reqêtes de page et les transmettre au bon contrôleur qui lui-même renverra la bonne vue.

# Organiser en dossiers
Il peut être interessant d'organiser ses fichiers par fonctions, c'est-à-dire regrouper tous les contrôleurs ensembles, etc
Également, on peut organiser son dossier par section de site  ou un découpage frontend/backend

# Gérer les erreurs
Les exceptions sont un moyen de bien gérer les erreurs. Pour générer une erreur il faut jeter (throw) une exception avec la syntaxe:

	throw new Exception('Message');

Lorsqu'une erreur se produit dans une sous-fonction, cette erreur est remontée dans le bloc catch le plus haut.

