# Qu'est-ce que la programmation orientée objet ?

Un objet, c'est un ensemble de variables et de fonctions. La programmation orientée objet, c'est créer du code source mais que l'on masque en le plaçant ç l'intérieur d'un objet pour simplifier son utilisation.
Pour créer un objet, on définit une classe: il s'agit du plan de construction de l'objet. On ne peut appeler que les éléments publics de la classe, cette façon de fonctionner est le principe d'encapsulation.
'un des concepts les plus importants s'appelle l'héritage, c'est-à-dire le fait de définir une classe par rapport à une autre.
On utilise des espaces de nom pour éviter les collisions des noms de classes.
