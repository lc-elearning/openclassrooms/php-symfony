# Pourquoi faire un code professionnel ?

## Pourquoi faire un code professionnel ?
Généralement, un code professionnel respecte les contraintes suivantes:
- il est modulaire (1 fichier = 1 role);
- il est découplé (les fichiers sont indépendants);
- il est documenté (PHPdoc);
- il est en anglais;
- il est clair (indenté, pas de fichier de plus de 1000 lignes);

Ce genre de code est plus facile à comprendre par le plus grand nombre, il est possible d'y travailler à plusiseurs sans trop d'effort et il est évolutif. PLus un code est compréhensible est plus il est réutilisable.

## Les étapes pour devenir un pro
La création d'un code pro passe par 3 grandes étapes:
- faire un code qui marche, *aka* un code qui **marche**;
- faire un code structuré et modulaire, *aka* ce cours;
- faire un code performant sans devoir tout recoder à chaque projet, *aka* utiliser un **framework**.

# Isoler l'affichage du traitement PHP

On va en premier vouloir séparer le traitement de la requête de son affichage:

- on utilise un fichier ```index.php``` qui va se connecter à la base de donnée et récupérer les informations nécessaires;
- on utilise un fichier ```affichageAccueil.php``` qui va afficher les données récupérer en formattant le HTML.

Pour cela on va inclure la page d'affichage dnas la page de traitement à l'aide de l'instruction "require();".

	index.php
	<?php
	try
	{
    		$bdd = new PDO('mysql:host=localhost;dbname=test;charset=utf8', 'root', 'root');
	}
	catch(Exception $e)
	{
    		die('Erreur : '.$e->getMessage());
	}
	
	$req = $bdd->query('SELECT id, titre, contenu, DATE_FORMAT(date_creation, \'%d/%m/%Y à %Hh%imin%ss\') AS date_creation_fr FROM billets ORDER BY date_creation DESC LIMIT 0, 5');
	
	require('affichageAccueil.php');
	?>

# Isoler l'accès aux données

Cette étape va organiser notre code en trois fichiers:

- `modele.php` ce fichier va servir à la connection à la base de données et à la récupération du contenu;
- `affichageAccueil.php` ce fichier va servir ) l'affichage de la page (aucun) changement;
- `index.php` ce fichier fait le lien entre les 2 fichiers précedents.

Ces 3 fichiers forment la base d'une structure MVC:

- **le modèle** qui va traiter les données -> `modele.php`;
- **la vue** qui va afficher les données -> `affichageAccueil.php`;
- **le contrôleur** qui fait le lien -> `index.php`.

